# 微信小程序代码片段

## 1. 左右滑动切换选项卡
![左右滑动切换选项卡](./img/tab.gif "效果图如上")

## 2. 图片自适应

- 给图片添加一个方法`bindload='imageLoad'`来根据不同设备设置图片狂高

    ```javascript
        imageLoad: function (e) {
            var imageSize = imageUtil.imageUtil(e)
            this.setData({
            imagewidth: imageSize.imageWidth,
            imageheight: imageSize.imageHeight
        })

    ```
![图片自适应](./img/banner_img.jpg "效果图如上")