//index.js
//获取应用实例
var imageUtil = require('../../utils/imageUtil')
const app = getApp()

Page({
  data: {
    imgUrls: [
      'http://img02.tooopen.com/images/20150928/tooopen_sy_143912755726.jpg',
      'http://img06.tooopen.com/images/20160818/tooopen_sy_175866434296.jpg',
      'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg'
    ],
    indicatorDots: true,
    circular:true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    imagewidth: 0,
    imageheight: 0
  },
  //轮播图自适应
  imageLoad: function (e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight
    })
  },
  onLoad: function (options) {
    
  },

  onReady: function () {
    
  },

 
  onShow: function () {
    
  },

  onHide: function () {
    
  },

  onUnload: function () {
    
  },

  onPullDownRefresh: function () {
    
  },

 
  onReachBottom: function () {
    
  },

  onShareAppMessage: function () {
    
  }
})